/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/smartsocket',
  version: '2.0.19',
  description: 'easy and secure websocket communication'
}
