// node native
import type * as http from 'http';
import type * as https from 'https';

export { http, https };

// pushrocks scope
import type * as smartexpress from '@pushrocks/smartexpress';

export { smartexpress };

// third party scope
import type { Socket as ServerSocket, Server as ServerServer } from 'socket.io';
import type { Socket as ClientSocket, connect as ClientIo } from 'socket.io-client';

export namespace socketIo {
  export type Socket = ServerSocket;
  export type Server = ServerServer;
}
export namespace socketIoClient {
  export type Socket = ClientSocket;
  export type connect = typeof ClientIo;
}
